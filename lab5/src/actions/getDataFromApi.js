import React from "react"


export default function getDataFromApi() {

    return async (dispatch) => {
        fetch("https://dog.ceo/api/breeds/image/random")
        .then(respons =>  Response.json())
        .then(respons=>{
            console.log(respons)
            dispatch({ type: "addData", payload: respons })
        })
    }

}