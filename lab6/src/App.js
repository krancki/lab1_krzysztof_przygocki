import React from 'react';
import {Provider} from "react-redux";
import myStore from "./state"
import MainContener from "./conteners/MainContener";


function App() {
  return (
    <Provider store={myStore} >
    <MainContener></MainContener>
    </Provider>
  );
}

export default App;
